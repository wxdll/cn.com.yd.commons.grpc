package cn.com.yd.commons.grpc.annotation;

import cn.com.yd.commons.grpc.config.GrpcClientAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(GrpcClientAutoConfiguration.class)
public @interface EnableStartGrpcClient {
}
