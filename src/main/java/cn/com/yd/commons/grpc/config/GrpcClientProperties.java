package cn.com.yd.commons.grpc.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "grpc.client.options")
public class GrpcClientProperties {
    private String serverIp;

    private Integer port;

    private Boolean enabledSsl;

    private Integer maxTotal;

    private Integer maxWait;

    private Boolean onBorrow;

    private Boolean onReturn;

    private Boolean whileIdle;

    private Integer rate;

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Boolean getEnabledSsl() {
        return enabledSsl;
    }

    public void setEnabledSsl(Boolean enabledSsl) {
        this.enabledSsl = enabledSsl;
    }

    public Integer getMaxTotal() {
        return maxTotal;
    }

    public void setMaxTotal(Integer maxTotal) {
        this.maxTotal = maxTotal;
    }

    public Integer getMaxWait() {
        return maxWait;
    }

    public void setMaxWait(Integer maxWait) {
        this.maxWait = maxWait;
    }

    public Boolean getOnBorrow() {
        return onBorrow;
    }

    public void setOnBorrow(Boolean onBorrow) {
        this.onBorrow = onBorrow;
    }

    public Boolean getOnReturn() {
        return onReturn;
    }

    public void setOnReturn(Boolean onReturn) {
        this.onReturn = onReturn;
    }

    public Boolean getWhileIdle() {
        return whileIdle;
    }

    public void setWhileIdle(Boolean whileIdle) {
        this.whileIdle = whileIdle;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }
}
